#####################################################
# dolby plus decoder
# insert noise
####################################################

#DOLBY_DS1_UDC
ifdef DOLBY_DS1_UDC

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_PREBUILT_LIBS := libstagefright_soft_ddpdec.so

LOCAL_MODULE_TAGS := optional

include $(BUILD_MULTI_PREBUILT) 

endif
#DOLBY_DS1_UDC_END
