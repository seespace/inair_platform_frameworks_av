/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file has been derived by Dolby Laboratories from AAMRAssembler.h. 
 * This file can be re-distributed under Apache License Version 2.0.
 *
 */

#ifndef A_DDP_ASSEMBLER_H_

#define A_DDP_ASSEMBLER_H_

#include "ARTPAssembler.h"

#include <utils/List.h>
#include <utils/RefBase.h>

namespace android {

struct ABuffer;
struct AMessage;
struct AString;

struct ADDPAssembler : public ARTPAssembler {
    ADDPAssembler(const sp<AMessage> &notify, const AString &desc, const AString &params);

protected:
    virtual ~ADDPAssembler();

    virtual AssemblyStatus assembleMore(const sp<ARTPSource> &source);
    virtual void onByeReceived();
    virtual void packetLost();

private:
    sp<AMessage> mNotifyMsg;

    bool mNextExpectedSeqNoValid;
    uint32_t mNextExpectedSeqNo;

    AssemblyStatus addPacket(const sp<ARTPSource> &source);

    static int calc_dd_frame_size(int code);
    static bool IsSeeminglyValidDDPAudioHeader(const uint8_t *ptr, size_t size);

    DISALLOW_EVIL_CONSTRUCTORS(ADDPAssembler);

};

} // namespace android

#endif // A_DDP_ASSEMBLER_H_
