LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:=       \
        AAMRAssembler.cpp           \
        AAVCAssembler.cpp           \
        AH263Assembler.cpp          \
        AMPEG2TSAssembler.cpp       \
        AMPEG4AudioAssembler.cpp    \
        AMPEG4ElementaryAssembler.cpp \
        APacketSource.cpp           \
        ARawAudioAssembler.cpp      \
        ARTPAssembler.cpp           \
        ARTPConnection.cpp          \
        ARTPSource.cpp              \
        ARTPWriter.cpp              \
        ARTSPConnection.cpp         \
        ASessionDescription.cpp     \
        SDPLoader.cpp               \

ifdef DOLBY_UDC
ifdef DOLBY_UDC_STREAMING_RTSP
  LOCAL_SRC_FILES += ADDPAssembler.cpp
endif
endif #DOLBY_UDC
LOCAL_C_INCLUDES:= \
	$(TOP)/frameworks/av/media/libstagefright/include \
	$(TOP)/frameworks/native/include/media/openmax \
	$(TOP)/external/openssl/include
    
ifdef DOLBY_UDC
    LOCAL_C_INCLUDES += $(TOP)/frameworks/av/media/libstagefright/
endif #DOLBY_UDC
LOCAL_MODULE:= libstagefright_rtsp
ifdef DOLBY_UDC
ifdef DOLBY_UDC_STREAMING_RTSP
  LOCAL_CFLAGS += -DDOLBY_UDC
  LOCAL_CFLAGS += -DDOLBY_UDC_STREAMING_RTSP
endif
endif #DOLBY_UDC

ifeq ($(TARGET_ARCH),arm)
    LOCAL_CFLAGS += -Wno-psabi
endif

include $(BUILD_STATIC_LIBRARY)

################################################################################

include $(CLEAR_VARS)

LOCAL_SRC_FILES:=         \
        rtp_test.cpp

LOCAL_SHARED_LIBRARIES := \
	libstagefright liblog libutils libbinder libstagefright_foundation

LOCAL_STATIC_LIBRARIES := \
        libstagefright_rtsp

LOCAL_C_INCLUDES:= \
	frameworks/av/media/libstagefright \
	$(TOP)/frameworks/native/include/media/openmax

LOCAL_CFLAGS += -Wno-multichar

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= rtp_test

# include $(BUILD_EXECUTABLE)
