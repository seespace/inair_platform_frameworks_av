LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    ISchedulingPolicyService.cpp \
    SchedulingPolicyService.cpp

# FIXME Move this library to frameworks/native
LOCAL_MODULE := libscheduling_policy

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:=               \
    AudioFlinger.cpp            \
    Threads.cpp                 \
    Tracks.cpp                  \
    Effects.cpp                 \
    AudioMixer.cpp.arm          \
    AudioResampler.cpp.arm      \
    AudioPolicyService.cpp      \
    ServiceUtilities.cpp        \
    AudioResamplerCubic.cpp.arm \
    AudioResamplerSinc.cpp.arm

LOCAL_SRC_FILES += StateQueue.cpp

LOCAL_C_INCLUDES := \
    $(call include-path-for, audio-effects) \
    $(call include-path-for, audio-utils)

LOCAL_SHARED_LIBRARIES := \
    libaudioutils \
    libcommon_time_client \
    libcutils \
    libutils \
    liblog \
    libbinder \
    libmedia \
    libnbaio \
    libhardware \
    libhardware_legacy \
    libeffects \
    libdl \
    libpowermanager

LOCAL_STATIC_LIBRARIES := \
    libscheduling_policy \
    libcpustats \
    libmedia_helper

LOCAL_MODULE:= libaudioflinger

LOCAL_SRC_FILES += FastMixer.cpp FastMixerState.cpp AudioWatchdog.cpp

LOCAL_CFLAGS += -DSTATE_QUEUE_INSTANTIATIONS='"StateQueueInstantiations.cpp"'

# Define ANDROID_SMP appropriately. Used to get inline tracing fast-path.
ifeq ($(TARGET_CPU_SMP),true)
    LOCAL_CFLAGS += -DANDROID_SMP=1
else
    LOCAL_CFLAGS += -DANDROID_SMP=0
endif

ifdef DOLBY_DAP
    # DAP log level for AudioFlinger service: 0=1=Off, 2=Intense, 3=Verbose
    LOCAL_CFLAGS += -DDOLBY_DAP_LOG_LEVEL_AUDIOFLINGER=3
ifdef DOLBY_DAP_OPENSLES
    LOCAL_CFLAGS += -DDOLBY_DAP_OPENSLES
    # DAP compilation switch for applying the pregain
    # Note: Keep this definition consistent with Android.mk in DS effect
    LOCAL_CFLAGS += -DDOLBY_DAP_OPENSLES_PREGAIN

ifdef DOLBY_DAP_OPENSLES_LPA
    LOCAL_CFLAGS += -DDOLBY_DAP_OPENSLES_LPA
endif

ifdef DOLBY_DAP_OPENSLES_LPA_TEST
    LOCAL_CFLAGS += -DDOLBY_DAP_OPENSLES_LPA_TEST
endif

else ifdef DOLBY_DAP_DSP
    LOCAL_CFLAGS += -DDOLBY_DAP_DSP
    # DAP compilation switch for applying the pregain
    # Note: Keep this definition consistent with Android.mk in DsNative
    LOCAL_CFLAGS += -DDOLBY_DAP_DSP_PREGAIN

    LOCAL_C_INCLUDES += $(TOP)/frameworks/base/dolby/include
    LOCAL_SHARED_LIBRARIES += libds_native
endif
endif # DOLBY_DAP

LOCAL_CFLAGS += -fvisibility=hidden

include $(BUILD_SHARED_LIBRARY)

#
# build audio resampler test tool
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES:=               \
	test-resample.cpp 			\
    AudioResampler.cpp.arm      \
	AudioResamplerCubic.cpp.arm \
    AudioResamplerSinc.cpp.arm

LOCAL_SHARED_LIBRARIES := \
    libdl \
    libcutils \
    libutils \
    liblog

LOCAL_MODULE:= test-resample

LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

include $(call all-makefiles-under,$(LOCAL_PATH))
